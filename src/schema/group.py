# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from ariadne import ObjectType
from .filter_tasks import filter_tasks

group = ObjectType("Group")

@group.field("tasks")
def resolve_tasks(group, info, evaluator, worker, customer, stage):

    tasks = group.tasks

    tasks = filter_tasks(tasks, evaluator=evaluator, worker=worker, customer=customer, stage)

    return tasks
