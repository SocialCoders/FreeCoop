# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from ariadne import MutationType
from .auth import auth_required
from flask import request
from exceptions import InvalidGroupMember, InvalidUserError
import base64
import db

mutation = MutationType()

# SIGNUP

@mutation.field("signup")
def resolve_signup(_, info, name, email, password):
    user = db.User.signup(name, email, password)

    return user

# LOGIN
@mutation.field("authorize")
def resolve_authorize(_, info, email_or_name, password):
    return db.AccessToken.generate(email_or_name, password)

@mutation.field("invalidate")
@auth_required
def resolve_invalidate(_, info, access_token):
    user = db.AccessToken.get_user(request.headers.get("Authorization"))
    access_token = db.AccessToken.get_or_none(db.AccessToken.token == base64.b64decode(access_token, b"-_") & db.AccessToken.user.id == user.id)

    if access_token is None:
        return None

    return access_token.delete_instance()

# PAYMENT INFO

@mutation.field("set_payment_info")
@auth_required
def resolve_set_payment_info(_, info, payment_method, payment_address):
    user = db.AccessToken.get_user(request.headers.get("Authorization"))

    if payment_method is not None:
        user.payment_method = payment_method

    if payment_address is not None:
        user.payment_address = payment_address

    user.save()

    return user

# MEMBERSHIP

@mutation.field("apply_for_membership")
@auth_required
def resolve_apply_for_membership(_, info, group_id):
    user = db.Access.get_user(request.headers.get("Authorization"))
    group = db.Group.get_by_id(group_id) # raises DoesNotExist error

    if group.is_member(user):
        raise InvalidGroupMember(group_id) # already a member

    user.membership = Membership.GUEST
    user.group = group
    user.save()

    return user

@mutation.field("accept_trial_membership")
@auth_required
def resolve_accept_trial_membership(_, info, user_id):
    moderator = db.AccessToken.get_user(request.headers.get("Authorization"))
    group = moderator.group

    user = db.User.get_by_id(user_id) # raises DoesNotExist error

    if not group.has_membership(user, Membership.GUEST):
        raise InvalidUserError(user.id)

    if not group.has_membership(moderator, Membership.MODERATOR):
        raise InvalidRoleError(moderator.membership)

    user.membership = Membership.TRIAL
    user.save()

    return group



