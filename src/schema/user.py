# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from ariadne import ObjectType
from flask import request
import db

user = ObjectType("User")

@user.field("payment_method")
def resolve_payment_method(user, info):
    requester = db.AccessToken.get_user(request.headers.get("Authorization"))

    if user.can_access_payment_info(requester):
        return user.payment_method

    return None

@user.field("payment_address")
def resolve_payment_address(user, info):
    requester = db.AccessToken.get_user(request.headers.get("Authorization"))

    if user.can_access_payment_info(requester):
        return user.payment_address
    return None

@user.field("tasks")
def resolve_tasks(user, info, primary_evaluator, primary_worker, evaluator, worker, customer, stage):

    tasks = db.Task.select().where(user << db.Task.evaluators | user << db.Task.workers)

    if primary_evaluator is not None:
        tasks = tasks.where(user == db.Task.primary_evaluator)

    if primary_worker is not None:
        tasks = tasks.where(user == db.Task.primary_worker)

    if evaluator is not None:
        tasks = tasks.where(user << db.Task.evaluators)

    if worker is not None:
        tasks = tasks.where(user << db.Task.workers)

    if customer is not None:
        tasks = tasks.where(user == db.Task.customer)

    if stage is not None:
        tasks = tasks.where(db.Task.stage == stage)

    return tasks

