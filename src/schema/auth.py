# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from flask import request
from functools import wraps
import db
from exceptions import *

def auth_required(fn):
    @wraps(fn)
    def inner(*args, **kwargs):
        if db.AccessToken.is_valid(request.headers.get("Authorization", None)):
            return fn(*args, **kwargs)
        else:
            raise InvalidAccessToken

    return inner
