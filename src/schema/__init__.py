# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from ariadne import load_schema_from_path, make_executable_schema
from .query import query
from .mutation import mutation
from .user import user
from .group import group

schema = make_executable_schema(load_schema_from_path("../graphql"), [query, mutation, user, group])

