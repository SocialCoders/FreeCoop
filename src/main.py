# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

"""
Provides the flask app object.
"""
from flask import Flask, request, jsonify
from ariadne import graphql_sync
from ariadne.constants import PLAYGROUND_HTML
from schema import schema
import os
import db

app = Flask(__name__)
app.debug = "DEBUG" in os.environ

@app.before_request
def before_request():
    """Connect to the Database
    """
    if db.database.is_closed():
        db.database.connect()

@app.after_request
def after_request(response):
    """Disconnect from the databse, if the connection exists.
    """
    if not db.database.is_closed():
        db.database.close()

    return response

@app.route("/api/graphql", methods=["GET"])
def graphql_playground():
    return PLAYGROUND_HTML, 200


@app.route("/api/graphql", methods=["POST"])
def graphql():

    data = request.get_json()
    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug,
    )

    status_code = 200 if success else 400
    return jsonify(result), status_code
