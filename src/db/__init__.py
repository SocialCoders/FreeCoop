# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from .BaseModel import database
from .User import User, AccessToken, Membership
from .Group import Group, Budget
from .Vote import Vote, Proposal, Ballot, Participant
from .Task import Task, TaskProposal
import os

if "CREATE_TABLES" in os.environ:
    database.connect()
    database.create_tables([User, AccessToken, Group, Task, TaskProposal])
    database.close()

