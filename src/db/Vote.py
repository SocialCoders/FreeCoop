# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
from .BaseModel import BaseModel
from .User import User, Membership
from .Group import Group
from enum import Enum
from .enum_field import EnumField
import peewee as pw

class VoteType(Enum):
    GENERIC = 0

    ACCEPT_MEMBER = 1
    REMOVE_MEMBER = 2

    CANDIDACY = 3
    IMPEACHMENT = 4

    BUDGET = 5


class Vote(BaseModel):
    id = pw.UUIDField(primary_key=True)

    # time: Start of the Vote, start of the voting phase and end of the vote.
    # Phases:
    # start - start of voting: proposal phase
    # start of voting - end: voting phase
    start = pw.IntegerField()
    start_voting = pw.IntegerField()
    end = pw.IntegerField()

    vote_type = EnumField(VoteType)

    # Depending on the vote type the franchise may differ:
    # 1. For candidacy, impeachment and any other appointment votes, the permanent members and appointees are the franchise.
    # 2. For budget votes, all permanent, trial and appointees are the franchise
    # 3. For the first vote to remove a member, the moderators only are the franchise.
    # 4. For any vote to accept a member and for the second removal vote all permanent members and appointees are the franchise.
    franchise = pw.ManyToManyField(User, backref="permitted_votes")

class Proposal(BaseModel):
    id = pw.UUIDField(primary_key=True)
    time = pw.IntegerField()
    proposer = pw.ForeignKeyField(User, backref="proposals")

    proposal_type = EnumField(VoteType)
    payload = pw.BlobField()

class Ballot(BaseModel):
    id = pw.UUIDField(primary_key=True)
    vote = pw.ForeignKeyField(Vote, backref="ballots")

    ballot = pw.BlobField() # [Proposal ID]

class Participant(BaseModel):
    """Participants MUST be stored to
    prevent double voting.

    To allow users to change their votes, the participant record
    refers to a voter for a minimum of 24h after the last change to the
    ballot.

    Both Participant.time and Participant.ballot field are deleted
    24h the last ballot change or when voting closes.
    """
    id = pw.UUIDField(primary_key=True)

    ballot = pw.ForeignKeyField(Ballot, backref="participant", null=True)
    voter = pw.ForeignKeyField(User, backref="voting_participation")

    time = pw.IntegerField(null=True)



