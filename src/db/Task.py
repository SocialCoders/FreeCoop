# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
from enum import Enum
from .enum_field import EnumField
from .BaseModel import BaseModel
from .Group import Group
from .User import User, Membership
import peewee as pw

# Task

class Stage(Enum):
    PROPOSED = 1
    APPROVED = 2
    ASSIGNED = 3
    COMPLETED = 4

class Task(BaseModel):
    id = pw.UUIDField(primary_key=True)
    group = pw.ForeignKeyField(Group, backref="tasks")

    customer = pw.ForeignKeyField(User, backref="tasks_as_customer")

    primary_evaluator = pw.ForeignKeyField(User, backref="tasks_as_primary_evaluator") # consent required to add evaluators
    primary_worker = pw.ForeignKeyField(User, backref="tasks_as_primary_worker") # consent required to add workers

    evaluators = pw.ManyToManyField(User, backref="tasks_as_evaluator") # majority of evaluators consent required for approval
    workers = pw.ManyToManyField(User, backref="tasks_as_worker") # majority of workers consent required for completion

    stage = EnumField(Stage)

    customer_proposal = pw.DeferredForeignKey("TaskProposal", backref="task")
    evaluators_proposal = pw.DeferredForeignKey("TaskProposal", backref="task")

    accepted_proposal = pw.DeferredForeignKey("TaskProposal", backref="task") # accepted by both the majority of evaluators and the customer.

class TaskProposal(BaseModel):
    proposers = pw.ManyToManyField(User, backref="proposals")

    description = pw.TextField()

    payment = pw.IntegerField()
    escrow_fee = pw.IntegerField()

    accepted_by = pw.ManyToManyField(User, backref="accepted_proposals")

class Payment(BaseModel):
    id = pw.UUIDField(primary_key=True)
    receiver = pw.ForeignKeyField(User, backref="receiving")
    payer = pw.ForeignKeyField(User, backref="payments")

    reason = pw.TextField()
    completed = pw.BooleanField(default=False)

class Transaction(BaseModel):
    payment = pw.ForeignKeyField(Payment, backref = "transactions")
    id = pw.TextField()

    src = pw.TextField()
    dest = pw.TextField()
    amount = pw.IntegerField()

    completed = pw.BooleanField(default=False)
