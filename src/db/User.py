# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
from enum import Enum
from .enum_field import EnumField
from .BaseModel import BaseModel
from Crypto.Hash import SHA3_256
from Crypto.Random import get_random_bytes
import peewee as pw
import base64
import uuid


class Membership(Enum):
    GUEST = 0
    TRIAL = 1
    PERMANENT = 2
    MODERATOR = 3
    TREASURER = 4

class PaymentMethod(Enum):
    BANK_TRANSFER = 0
    XMR = 1
    PAYPAL = 2

class User(BaseModel):
    id = pw.UUIDField(primary_key=True)

    password_hash = pw.BlobField()
    salt = pw.BlobField()

    group = pw.DeferredForeignKey("Group", null = True, backref="members")
    membership = EnumField(Membership, null=True)

    name = pw.CharField()
    email = pw.CharField(unique=True)
    payment_address = pw.TextField(null=True)
    payment_method = EnumField(PaymentMethod, null=True)

    def signup(name, email, password):
        salt = get_random_bytes(32)
        password_hash = SHA3_256.new(password.encode("utf-8") + salt).digest()

        # generate a unique id.
        id = None
        while id is None or User.get_or_none(User.id == id) is not None:
            id = uuid.uuid4()

        return User.create(
                id=id,
                password_hash=password_hash,
                salt=salt,
                name=name,
                email=email)

    def apply_group(self, group):
        self.group = group
        self.membership = Membership.GUEST

    def accept_trial_member(self, user):
        if user.group.id != self.group.id or user.membership is not Membership.GUEST:
            raise InvalidUserError(user)


        if self.membership is not Membership.MODERATOR:
            raise InvalidRoleError(self.membership)

        user.membership = Membership.TRIAL
        user.save()

    def can_access_payment_info(self, requester):
        """Only the user themselves and their group treasurer
        can access the payment info. The treasurer needs the
        payment info to be able to fulfill their responsibilities.
        """
        if requester == self:
            return True

        if self.group is not None and requester.group == self.group and requester.membership == Membership.TREASURER:
            return True

class AccessToken(BaseModel):
    token = pw.CharField(primary_key=True)
    token_type = pw.CharField()
    user = pw.ForeignKeyField(User, backref="access_tokens")
    expires = pw.IntegerField()


    def generate(email_or_name, password):
        user = User.get_or_none((User.name == email_or_name) | (User.email == email_or_name))

        if user is None:
            raise InvalidEmailOrNameError(email_or_name)

        if user.password_hash != SHA3_256.new(password.encode("utf-8") + user.salt).digest():
            raise InvalidPasswordError()

        token = base64.b64encode(get_random_bytes(32), b"-_").decode("utf-8")
        expires = round(time.time() + 43200) # 60*60*12

        return AccessToken.create(
            token=token,
            token_type="Bearer",
            user=user,
            expires=expires)

    def is_valid(token):
        if token is None:
            return False

        token_type, token = token.split(" ")[0:2]
        token = AccessToken.get_or_none((AccessToken.token == token) & (AccessToken.token_type == token_type))

        if token_type not in ["Bearer"]:
            return False

        if token is None:
            return False

        return time.time() < token.expires

    def get_user(token):
        """Called upon a valid token to fetch the token's user.
        """
        token_type, token  = token.split(" ")[0:2]
        token = AccessToken.get_or_none((AccessToken.token == token) & (AccessToken.token_type == token_type))

        if token is None:
            return None

        return token.user
