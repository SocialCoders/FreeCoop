# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

import peewee as pw

class EnumField(pw.Field):
    field_type = "text"

    def __init__(self, enum, *args, **kwargs):
        super(EnumField, self).__init__(*args, **kwargs)

        self.__enum = enum

    def db_value(self, value):
        if value is None:
            return None
        return value.name

    def python_value(self, value):
        if value is None:
            return None
        return self.__enum[value]
