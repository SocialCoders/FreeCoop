# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

from .BaseModel import BaseModel
from .User import User, Membership
import peewee as pw
from enum import Enum
from .enum_field import EnumField

# Budget
class Budget(BaseModel):
    group = pw.DeferredForeignKey("Group", backref="budgets")

    accepted = pw.BooleanField(default=False)
    time = pw.IntegerField()
    year = pw.IntegerField()

    monthly = pw.IntegerField()

    evaluator_reward = pw.DecimalField(max_digits=40, decimal_places=39)
    worker_reward = pw.DecimalField(max_digits=40, decimal_places=39)

    purposes = pw.TextField() # purposes that the treasurer can use the budget for

# Group
class Group(BaseModel):
    id = pw.UUIDField(primary_key=True)

    name = pw.CharField()
    website = pw.TextField()

    budget = pw.ForeignKeyField(Budget, null=True)

    def is_member(self, user):
        """Is a user a member of the group, regardless
        of their membership.
        """
        return user.group is not None and user.group.id == self.id

    def has_membership(self, user, membership: Membership):
        """Is a user a member and has a certain membership?
        """
        return self.is_member(user) and user.membership is membership







