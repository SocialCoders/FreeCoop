# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

class InvalidUserError(Exception):
    def __init__(self, user):
        self.__user = user

class InvalidRoleError(Exception):
    def __init__(self, role):
        self.__role = role

class InvalidEmailOrNameError(Exception):
    def __init__(self, email_or_name):
        self.__email_or_name = email_or_name

class InvalidAccessToken(Exception):
    def __init__(self):
        pass

class InvalidGroupMember(Exception):
    def __init__(self, id):
        self.__id__ = id
